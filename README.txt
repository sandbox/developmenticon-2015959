DESCRIPTION
-----------

LayerSlider module can be used to to create and configure LayerSlider
of multiple layers of image, text and video all with transitions, timing and animation.


You can try out live demonstration of it here:
http://kreaturamedia.com/layerslider-responsive-jquery-slider-plugin/


INSTALLATION
------------

1. Follow the usual module installation procedure (2).
2. Visit the settings page for global settings.

[2] http://drupal.org/documentation/install/modules-themes/modules-7
[3] admin/config/media/layerslider

INSTALLATION
------------

1. Download the LayerSlider library from http://codecanyon.net/item/layerslider-responsive-jquery-slider-plugin/922100
2. Unzip the file and rename the folder to "layerslider" (pay attention to the case of the letters)
3. Put the folder in a libraries directory
    - Ex: sites/all/libraries
4. The first two files are required and the last is optional (required for javascript debugging)
    - jquery.flexslider-min.js
    - flexslider.css
    - jquery.flexslider.js
4. Ensure you have a valid path similar to this one for all files
    - Ex: sites/all/libraries/layerslider/jQuery/jquery-easing-1.3.js
          sites/all/libraries/layerslider/js/layerslider.kreaturamedia.jquery.js

That's it!


MAINTAINERS
------------
developmenticon.com, drupalchamp.com
